<?php
namespace Maowenke\WorkerManTask;
use think\Service as BaseService;

class Service extends BaseService{
    public function register(){
        $this->commands([
            'worker:task'=>'\\Maowenke\\WorkerManTask\\command\\MaoWorkerTask',
        ]);
    }
}